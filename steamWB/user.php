<!DOCTYPE html>
<html>
<head>
	<title></title>

<?php

	// Should print debug text?
	$debug = false;

	function getId64($input) {

		if (strlen($input) == 17 && strpos($input, "7656119") === 0)
		{
			if ($GLOBALS['debug']) echo "Input recognised as SteamID64 <br>";
			return $input;
		}

		if(strpos($input, "http://steamcommunity.com/id/") === 0)
			$url = $input;
		else
			$url = "http://steamcommunity.com/id/" . $input;

		$url = $url . "/?xml=1";
		if ($GLOBALS['debug']) echo "Accessing XML file " . $url . "<br>";

		$xml = simplexml_load_file($url);
   		return $xml->steamID64;

	}

	$input = trim($_GET["id"]);

?>

</head>
<body>

<?php

	if ($debug) echo "input: " . $input . "<br>";

	$id64 = getId64($input);

	if ($id64 == "")
	{
		echo "ERROR - Wrong input<br>";
		return;
	}

	if ($debug) echo "SteamID64: " . $id64 . "<br>";
	$apiKey = "272F0A764810C70218FFEA5A57DF4507";

	$url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?format=xml&key=" . $apiKey . "&steamids=" . $id64;
	if ($debug) echo "Accessing URL: " . $url . "<br><br>";

	$xml = simplexml_load_file($url);
	$currentGame = $xml->players->player->gameextrainfo;

	/*
	
		TO-DO LIST

			- Add Steam store link
			- Add PcGamingWiki link

			- Add Google search bar

			- Add time, date
			- Show game price
			- Show friends who own

	*/

	if ($currentGame)
	{
		$wikiURL = "http://pcgamingwiki.com/wiki/" . rawurlencode($currentGame);
		echo "Current game: " . $currentGame;
	}
	elseif ($xml->players->player->timecreated)
	{
		echo "Not in game<br>";
		return;
	}
	else
	{
		echo "Private profile - Please set profile to public for this tool to work<br>";
		return;
	}

	$htm = file_get_contents($wikiURL);

	$genInfoLoc = strpos($htm, "<b>General information</b>");
	$genInfoStart = strpos($htm, "<dl>", $genInfoLoc);
	$genInfoEnd = strpos($htm, "</dl>", $genInfoLoc);

	$infoList = substr($htm, $genInfoStart + 4, $genInfoEnd - $genInfoStart - 5);
	$infoList = str_replace("/images/", "http://pcgamingwiki.com/images/", $infoList);

	// Add Steam store link to start
	if ($xml->players->player->gameid)
	// Perform this check in case user is in non-steam game.
	{
		$infoList = "<dl><dd><span class=\"iconcontent\" title=\"More information\"><img alt=\"More information\" 
		src=\"http://pcgamingwiki.com/images/thumb/b/b0/Document_icon.svg/16px-Document_icon.svg.png\" width=\"16\" height=\"16\" style=\"vertical-align: text-bottom\" 
		srcset=\"http://pcgamingwiki.com/images/thumb/b/b0/Document_icon.svg/24px-Document_icon.svg.png 1.5x, http://pcgamingwiki.com/images/thumb/b/b0/Document_icon.svg/32px-Document_icon.svg.png 2x\"> 
		</span> <a rel=\"nofollow\" class=\"external text\" href=\"http://store.steampowered.com/app/" . $xml->players->player->gameid . "\">Steam Store Page</a></dd>" . $infoList;
	}
	else
		$infoList = "<dl>" . $infoList;

	// Add PCGamingWiki link to end
	$infoList = $infoList . "<dd><span class=\"iconcontent\" title=\"More information\"><img alt=\"More information\" 
	src=\"http://pcgamingwiki.com/images/thumb/b/b0/Document_icon.svg/16px-Document_icon.svg.png\" width=\"16\" height=\"16\" style=\"vertical-align: text-bottom\" 
	srcset=\"http://pcgamingwiki.com/images/thumb/b/b0/Document_icon.svg/24px-Document_icon.svg.png 1.5x, http://pcgamingwiki.com/images/thumb/b/b0/Document_icon.svg/32px-Document_icon.svg.png 2x\"> 
	</span> <a rel=\"nofollow\" class=\"external text\" href=\"http://pcgamingwiki.com/wiki/" . rawurlencode($currentGame) . "\">PCGamingWiki Page</a></dd></dl>";

	echo $infoList;
?>

</body>
</html>